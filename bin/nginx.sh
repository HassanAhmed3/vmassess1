apt -y update
apt -y install nginx nano

# curl localhost
cp /vagrant/nginindex.html /var/www/html/index.html


if curl -sL localhost | grep -i 'Bionic' >/dev/null 2>&1
then
  echo "NGINX Machines is working: x1 per message"
else
  echo "NGINX not working" 1>&2
  exit 1
fi

dpkg -l | grep "nginx" && echo "NGINX is installed "
