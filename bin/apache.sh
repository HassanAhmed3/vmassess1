#!/bin/bash
yum -y install httpd nano
systemctl start httpd
systemctl enable httpd

#cp /vagrant/vaindex.html /var/www/html/index.html
cp /vagrant/rhelwebauto /etc/init.d/rhelwebauto
sudo chmod +x rhelwebauto
sudo service rhelwebauto start

#chkconfig --add /etc/init.d/rhelwebauto


if curl -sL localhost | grep 'Apache' >/dev/null 2>&1
then
  echo "CENTOS Machines is working"
else
  echo "CENTOS not working" 1>&2
  exit 2
fi

rpm -qa | grep -i httpd && echo "httpd is installed "
